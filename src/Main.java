import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.
        System.out.printf("Hello and welcome!");

//        for (int i = 1; i <= 5; i++) {
//            //TIP Press <shortcut actionId="Debug"/> to start debugging your code. We have set one <icon src="AllIcons.Debugger.Db_set_breakpoint"/> breakpoint
//            // for you, but you can always add more by pressing <shortcut actionId="ToggleLineBreakpoint"/>.
//            System.out.println("i = " + i);
//        }

//            System.out.println("введите число ");
//
            Scanner scanner = new Scanner(System.in);
//            String str=scanner.nextLine();
//            //таким образом работает и ввод числа с , в качестве разделителя вместо . Но и . тоже работает...
//            System.out.println(Float.parseFloat(str.replaceAll(",",".")));
//
//            try {
//                System.out.println(Integer.parseInt(str));
//                System.out.println(Float.parseFloat(str));
//                System.out.println(Double.parseDouble(str));
//            } catch (NumberFormatException e){
//                System.out.println(e.getMessage());
//            }




//        Урок 4. Введение в типы данных
//        Цель задания:
//        Научится переводить переменные из одного типа в другой
//        Задания:
//        Во всех задачах исходные данные вводит пользователь:
        System.out.println("++++++++++++++++++++++++++ 1 ++++++++++++++++++++++++++++");
//        1.
//        Составьте программу, которая переводит переменную из byte в short
        System.out.println("Введите число");
        byte b1=scanner.nextByte();
        short sh1 = b1;
        System.out.println(sh1);
        System.out.println("++++++++++++++++++++++++++ 2 ++++++++++++++++++++++++++++");
//        2.
//        Составьте программу, которая переводит переменную из short в int
        System.out.println("Введите число");
        sh1 = scanner.nextShort();
        int i1 = sh1;
        System.out.println(i1);

        System.out.println("++++++++++++++++++++++++++ 3 ++++++++++++++++++++++++++++");
//        3.
//        Составьте программу, которая переводит переменную из int в long
        System.out.println("Введите число");
        i1 = scanner.nextInt();
        long l1 = i1;
        System.out.println(l1);

        System.out.println("++++++++++++++++++++++++++ 4 ++++++++++++++++++++++++++++");



//        4.
//        Составьте программу, которая переводит переменную из long в int
        System.out.println("Введите число");
        l1 = scanner.nextLong();
        i1 = (int)l1;
        System.out.println(l1);

        System.out.println("++++++++++++++++++++++++++ 5 ++++++++++++++++++++++++++++");




//        5.
//        Составьте программу, которая переводит переменную из float в double

        System.out.println("Введите число");
        Float f1 = scanner.nextFloat();
        Double d1 = (double)f1;
        System.out.println(d1);

        System.out.println("++++++++++++++++++++++++++ 6 ++++++++++++++++++++++++++++");

//        6.
//        Составьте программу, которая переводит переменную из double в float

        System.out.println("Введите число");
        d1 = scanner.nextDouble();
        f1 = d1.floatValue();
        System.out.println(f1);

        System.out.println("++++++++++++++++++++++++++ 7 ++++++++++++++++++++++++++++");


//        7.
//        Придумайте способ переводить из int в boolean и наоборот.
        System.out.println("Введите число");
        i1 = scanner.nextInt();
        Boolean bul1;
        if (i1>0){bul1=true;}
        else{bul1=false;}
        System.out.println(bul1);

        System.out.println("++++++++++++++++++++++++++ 8 ++++++++++++++++++++++++++++");



//        8.
//        Придумайте способ переводить из String в boolean.

        System.out.println("Введите строку");
        String str2 = new String(scanner.nextLine());
        if (str2.length() > 0){bul1=true;}
        else{bul1=false;}
        System.out.println(bul1);

        System.out.println("++++++++++++++++++++++++++ 9 ++++++++++++++++++++++++++++");



//        9.
//        Переведите переменную из char в string

        System.out.println("Введите символ");
        char ch1 = scanner.next().charAt(0);
        String str = String.valueOf(ch1);
        System.out.println(str);

        System.out.println("++++++++++++++++++++++++++ 10 ++++++++++++++++++++++++++++");

//        10.
//        Переведите переменную из char в int
//        и наоборот.
        System.out.println("Введите символ цифры");
        ch1 = scanner.next().charAt(0);
        i1 = Integer.parseInt(String.valueOf(ch1));
        System.out.println(i1);

        System.out.println("++++++++++++++++++++++++++ 11 ++++++++++++++++++++++++++++");



//        11.
//        Пусть int равен от 30 до 150. Переведите каждый int в char, и выведите
//        таблицу, где на каждой строчке есть int и соответствующий ему char

        for (int i2 = 30; i2 < 151; i2++){
            ch1=(char)i2;
            System.out.println("int = "+i2+" ; char ="+String.valueOf(ch1));
        }


//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написан скелет кода для вывода данных, с
//        оздана начальная
//        переменная
//        3 балла
//                -
//                выполнено более 60% заданий, имеется не более 5 критичных
//                замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считаетс
//        я выполненным при условии, что слушатель получил оценку не
//        менее 3 баллов




    }
}